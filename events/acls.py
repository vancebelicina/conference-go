from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:

        headers = {"Authorization": PEXELS_API_KEY}
        url = "https://api.pexels.com/v1/search?query=" + str(city) + "+" + str(state)
        r = requests.get(url, headers=headers)
        photos = json.loads(r.content)
        if len(photos) == 0:
            return None
        photo_dict = {
            "picture_url": photos["photos"][0]["url"]
        }
        return photo_dict
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    headers = {"Authorization": PEXELS_API_KEY}
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},unitedstates&limit=5&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    geo_response = requests.get(geo_url, headers=headers)
    # Parse the JSON response
    geo_parsed_json = json.loads(geo_response.content)
    # Get the latitude and longitude from the response
    if len(geo_parsed_json) == 0:
        return None
    lat = geo_parsed_json[0]["lat"]
    lon = geo_parsed_json[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # Make the request
    weather_response = requests.get(weather_url, headers=headers)
    # Parse the JSON response
    weather_parsed_json = json.loads(weather_response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    weather_dict = {
        "temp": weather_parsed_json["main"]["temp"],
        "description": weather_parsed_json["weather"][0]["description"]

    }
    # Return the dictionary
    return weather_dict
